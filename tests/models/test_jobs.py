import pytest

import dypi


def test_jobs_create():
    job1 = dypi.Job("job1")
    job2 = dypi.Job("job2")
    job3 = dypi.Job("job3")


def test_jobs_in_pipeline():

    base1 = dypi.Job("base1")
    base1.script.append(
        "This is base1's first line",
        "Here's line 2 of base1",
    )

    job1 = dypi.Job("job1")
    job1.script.append("job1 script")
    job1.extends(base1)
    base1.script.prepend("this is prepended")

    job2 = dypi.Job("job2")
    job2.script.append("job 2 script\nIs a multi-\nline-\nscript", "job2 line 2")

    job3 = dypi.Job("job3")
    job3.script.append("job3 script")
    job3.extends(job1)

    pipeline1 = dypi.Pipeline()
    pipeline1.add_children(job1, job2, job3)

    expected = """\
stages: []
variables: {}
workflow: []
job1:
  script:
    # Source job: base1
    - this is prepended
    - This is base1's first line
    - Here's line 2 of base1
    # Source job: job1
    - job1 script
job2:
  script:
    # Source job: job2
    - "job 2 script\\nIs a multi-\\nline-\\nscript"
    - job2 line 2
job3:
  script:
    # Source job: base1
    - this is prepended
    - This is base1's first line
    - Here's line 2 of base1
    # Source job: job1
    - job1 script
    # Source job: job3
    - job3 script
"""

    assert expected == pipeline1.build_yaml()
