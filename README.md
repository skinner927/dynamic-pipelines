# Dynamic Pipelines (DyPi)

```
$ export PYTHONPATH="$PWD/src
$ export FLASK_APP=dypi.server
$ export FLASK_ENV=development
$ flask run
```

## External references

- [`.gitlab-ci.yml` keyword reference](https://docs.gitlab.com/ee/ci/yaml/)


## Rambling

From the perspective of GitLab, a "pipeline" is just a bunch of jobs collected
into a YAML file with a few global keywords (`default`, `include`, `stages`,
`variables` and `workflows`). DyPi calls this a `BuildResult`.

Conceptually, DyPi has Jobs and Pipelines. Pipelines are used to collect Jobs.
Pipelines may also collect other Pipelines. Jobs may collect other Jobs
(we call it "extend" because GitLab does too), but a Job may not collect a
Pipeline (only because it's conceptually confusing, not due to technical
limitations).

DyPi will "build" the pipeline to compile the final YAML which will be fed to
GitLab. A build must start from a single Pipeline. The Pipeline will
request all of its child Jobs and Pipelines to "build". During a build, a
Pipeline will return a `BuildResult` which is a list of Jobs and the global
keywords. A Job will merge itself with all the Jobs
it "extends" and return a `BuildResult` with a list of just one Job (the merged
result of the Job layered on top of the Jobs it extends). This way the build
interface is the same for everyone and the entire build process only deals with
`BuildResult` objects instead of worrying about how to process the child. This
gives great flexibility for later extending our models.

When a Pipeline builds all its children (Pipelines and Jobs), it will then
iterate through the resulting Job list and update the unique properties it may
have been assigned (`stage`, `rules`, `image`, `tags`, etc.).

----

EVERYTHING must be annotated


TODO: How should circular references work?? Too tired to brain that right now.
A Job may be extended by more than one Job and added to more than one Pipeline.
A Pipeline may be added to more than one Pipeline, but no object may be added
to itself or a descendent. 

class Buildable; THis will let jobs be callable to be built, which then means
jobs can actually just be functions too.


Properties applied to a Pipeline
will cascade down to ALL Jobs below

Because DyPi can be dynamically aware of all Jobs within a Pipeline, the
compiled YAML has no need to include the global `default` abd `variables`
keywords because as our pipelines . The `include` keyword will be ignored
mostly because "nested includes execute without context as a public user".




## TODO:

- Scripts load paths
- Everything should just implement the `__call__()` dunder, so we can have Jobs
  that are actually just functions.
- Specify a gitlab version to the API pls.
- Once URL works maybe allow the server to pull the repo/branch or maybe use
  the gitlab api to pull a dypi.yaml file out of the root of the project
  instead of using urls only??
- Can a pipeline include another pipeline by name only??
