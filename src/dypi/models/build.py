from __future__ import annotations

import abc
import dataclasses
from typing import Dict, Generic, Tuple, TypeVar, Union

T = TypeVar("T")

# TODO: Anyone use this??
UserKwargs = Dict[str, Union[str, int, bool]]


@dataclasses.dataclass
class BuildContext:
    """Passed to every build step to pass down build context."""

    # I have reasons to believe this is a bad idea
    # built_memo: Dict[int, object] = dataclasses.field(default_factory=dict)
    # """Memo of all created Build* object.
    # Key is `id(self)` and value is the built value.
    # Some objects should not be memoized such as Pipelines and Jobs."""


class BuildNamed:
    # Protocols are busted on 3.8 so can't subclass.
    # https://bugs.python.org/issue44806
    # https://github.com/python/cpython/pull/27545
    def __init__(self, name: str) -> None:
        super().__init__()
        self._name: str = name

    @property
    def name(self) -> str:
        return self._name


class Buildable(BuildNamed, Generic[T], metaclass=abc.ABCMeta):
    @staticmethod
    def name_from_build_stack(build_stack: Tuple[BuildNamed, ...] = ()) -> str:
        return ".".join(b.name for b in build_stack if b.name)

    @abc.abstractmethod
    def __call__(
        self, ctx: BuildContext, _build_stack: Tuple[BuildNamed, ...] = ()
    ) -> T:
        raise NotImplementedError()
