import abc
import itertools
from typing import Dict, Iterator, List, Optional, Union

SerializableDict = Dict[
    str, Union[str, int, bool, List[Union[str, int, bool]], "SerializableDict"]
]


class RenderCollection:
    def __init__(self, name: str) -> None:
        if not isinstance(name, str):
            raise TypeError(f"Invalid type for 'name', expected <str> got {type(name)}")
        if not name:
            raise ValueError("Name cannot be empty")
        self._name: str = name
        self._parent: Optional[RenderCollection] = None
        self._children: List[RenderCollection] = list()

        self._counter = itertools.count(1)

    def _add_children(self, *children: "RenderCollection") -> None:
        self._children.extend(children)
        for child in children:
            if child._parent is not None:
                raise ValueError(f"Already has a parent {child}")

    def _iter_children(self) -> Iterator["RenderCollection"]:
        for child in self._children:
            yield child
        # for child in self._children:
        #     yield from child.iter_children()

    @property
    def _root(self) -> "RenderCollection":
        root = self
        while root._parent:
            root = root._parent
        return root

    def render(self) -> SerializableDict:
        raise NotImplementedError()

    #
    # def render_self(self, base: SerializableDict) -> SerializableDict:
    #     return base
    #
    # def render(self) -> SerializableDict:
    #     base = dict()
    #     for chi
    #     return self.render_self(base)
