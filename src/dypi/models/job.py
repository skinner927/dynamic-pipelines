from __future__ import annotations

import dataclasses
from typing import List, Optional, Protocol, Tuple, TypeVar

from dypi.models.annotations import AnnotatedSourceString
from dypi.models.build import Buildable, BuildContext, BuildNamed
from dypi.models.job_rule import BuiltJobRule, JobRule
from dypi.models.script import BuiltScript, Script, ScriptArgs

T = TypeVar("T")


class JobScriptModifier:
    def __init__(self, prepend_list: List[Script], append_list: List[Script]) -> None:
        self._prepend_list = prepend_list
        self._append_list = append_list

    def append(self, script: ScriptArgs) -> None:
        self._append_list.append(Script(script))

    def prepend(self, script: ScriptArgs) -> None:
        self._prepend_list.append(Script(script))


@dataclasses.dataclass()
class BuiltJob:
    # TODO: Should extend AnnotatedSource
    full_name: str
    original_name: str
    after_script: List[BuiltScript] = dataclasses.field(default_factory=list)
    allow_failure: Optional[bool] = None
    coverage: Optional[str] = None
    # noinspection SpellCheckingInspection
    interruptible: Optional[str] = None  # This is how gitlab spells it
    script: List[BuiltScript] = dataclasses.field(default_factory=list)
    stage: Optional[str] = None
    rules: List[BuiltJobRule] = dataclasses.field(default_factory=list)
    tags: List[AnnotatedSourceString] = dataclasses.field(default_factory=list)

    def update(self, src: BuiltJob) -> None:
        self.after_script.extend(src.after_script)
        if src.allow_failure is not None:
            self.allow_failure = src.allow_failure
        if src.coverage is not None:
            self.coverage = src.coverage
        if src.interruptible is not None:
            # noinspection SpellCheckingInspection
            self.interruptible = src.interruptible
        self.script.extend(src.script)
        if src.stage is not None:
            self.stage = src.stage
        self.rules.extend(src.rules)
        self.tags.extend(src.tags)


class Job(Buildable[BuiltJob]):
    def __init__(self, name: str, *, stage: Optional[str] = None):
        name = (name or "").strip()
        if not name:
            raise ValueError("Job name cannot be empty")
        super().__init__(name)
        self._name: str = name
        self._child_jobs: List[Job] = list()
        # self._arguments = list()

        # Prepend lists will come before child scripts
        self._after_script_prepend: List[Script] = list()
        self._after_script_append: List[Script] = list()
        self._script_prepend: List[Script] = list()
        self._script_append: List[Script] = list()

        self.after_script = JobScriptModifier(
            self._after_script_prepend, self._after_script_append
        )
        self.allow_failure: Optional[bool] = None
        self.coverage: Optional[str] = None
        # noinspection SpellCheckingInspection
        self.interruptible: Optional[str] = None  # This is how gitlab spells it
        self.script = JobScriptModifier(self._script_prepend, self._script_append)
        self.stage: Optional[str] = stage
        self.rules: List[JobRule] = list()
        self.tags: List[str] = list()

    def extends(self, *jobs: Job) -> None:
        self._child_jobs.extend(jobs)

    def __call__(
        self, ctx: BuildContext, _build_stack: Tuple[BuildNamed, ...] = ()
    ) -> BuiltJob:
        new_build_stack = _build_stack + (self,)
        my_full_name = self.name_from_build_stack(new_build_stack)

        bj = BuiltJob(
            full_name=my_full_name,
            original_name=self.name,
        )
        # Start with the "prepend" lists, then children, then append.
        bj.after_script = [s(ctx, new_build_stack) for s in self._after_script_prepend]
        bj.script = [s(ctx, new_build_stack) for s in self._script_prepend]

        # update via our children
        for child in self._child_jobs:
            bj.update(child(ctx, new_build_stack))

        # Create a temporary BuiltJob for easy update
        tmp = BuiltJob(
            full_name=my_full_name,
            original_name=self.name,
        )
        tmp.allow_failure = self.allow_failure
        tmp.coverage = self.coverage
        # noinspection SpellCheckingInspection
        tmp.interruptible = self.interruptible
        tmp.stage = self.stage
        tmp.rules = [rule(ctx, new_build_stack) for rule in self.rules]
        tmp.tags = [AnnotatedSourceString(t, source=my_full_name) for t in self.tags]
        bj.update(tmp)

        # Finally use the "append" scripts
        bj.after_script.extend(
            [s(ctx, new_build_stack) for s in self._after_script_append]
        )
        bj.script.extend([s(ctx, new_build_stack) for s in self._script_append])

        return bj
