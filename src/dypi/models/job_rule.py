from __future__ import annotations

import dataclasses
from typing import Dict, List, Literal, Optional, Tuple, TypedDict, Union, cast

from dypi.models.annotations import AnnotatedSource
from dypi.models.build import Buildable, BuildContext, BuildNamed

_When_Literal = Literal[
    "on_success", "manual", "always", "on_failure", "delayed", "never"
]

_BuiltJobRuleYamlDict = TypedDict(
    "_BuiltJobRuleYamlDict",
    {
        "if": str,
        "changes": List[str],
        "exists": List[str],
        "allow_failure": bool,
        "when": str,
        "variables": Dict[str, str],
    },
    total=False,
)


@dataclasses.dataclass()
class BuiltJobRule(AnnotatedSource):
    source: str = ""

    if_cond: Optional[str] = None
    changes: Optional[List[str]] = None
    exists: Optional[List[str]] = None
    allow_failure: Optional[bool] = None
    when: Optional[_When_Literal] = None
    variables: Optional[Dict[str, str]] = None

    def to_yaml_dict(
        self,
    ) -> _BuiltJobRuleYamlDict:
        out = _BuiltJobRuleYamlDict()
        if self.if_cond:
            out["if"] = self.if_cond
        if self.changes:
            out["changes"] = self.changes
        if self.exists:
            out["exists"] = self.exists
        if self.allow_failure is not None:
            out["allow_failure"] = self.allow_failure
        if self.when:
            out["when"] = self.when
        if self.variables:
            out["variables"] = self.variables
        return out


class JobRule(Buildable[BuiltJobRule]):
    def __init__(self) -> None:
        super().__init__("job")
        self.if_cond: Optional[str] = None
        self.changes: Optional[List[str]] = None
        self.exists: Optional[List[str]] = None
        self.allow_failure: Optional[bool] = None
        self.when: Optional[_When_Literal] = None
        self.variables: Optional[Dict[str, str]] = None

    def __call__(
        self, ctx: BuildContext, _build_stack: Tuple[BuildNamed, ...] = ()
    ) -> BuiltJobRule:
        return BuiltJobRule(
            source=self.name_from_build_stack(_build_stack + (self,)),
            if_cond=self.if_cond,
            changes=self.changes.copy() if self.changes is not None else None,
            exists=self.exists.copy() if self.exists is not None else None,
            allow_failure=self.allow_failure,
            when=self.when,
            variables=self.variables.copy() if self.variables is not None else None,
        )
