import inspect


# TODO: Drop
class InterfaceOnly(object):
    """Ensures the class will not be directly instantiated."""

    __slots__ = ()

    def __new__(cls, *_, **__) -> "InterfaceOnly":  # type: ignore
        if inspect.getmro(cls)[1] is InterfaceOnly:
            raise TypeError(
                f"Cannot instantiate {cls.__name__} directly because it is {InterfaceOnly.__name__}"
            )
        return super().__new__(cls)
