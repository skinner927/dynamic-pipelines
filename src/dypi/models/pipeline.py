from __future__ import annotations

import dataclasses
import itertools
from collections import OrderedDict
from typing import Dict, List, Literal, Optional, Tuple, TypedDict, Union

import ruamel.yaml
import ruamel.yaml.comments
import ruamel.yaml.compat

from dypi import Job
from dypi.models.annotations import AnnotatedSource
from dypi.models.build import Buildable, BuildContext, BuildNamed, UserKwargs
from dypi.models.job import BuiltJob

C_SEQ = ruamel.yaml.comments.CommentedSeq
C_MAP = ruamel.yaml.comments.CommentedMap

_global_pipeline_counter = itertools.count(1)


@dataclasses.dataclass()
class WorkflowRule:
    if_cond: Optional[str] = None
    when: Optional[Literal["always", "never"]] = None
    variables: Optional[Dict[str, str]] = None


@dataclasses.dataclass()
class Workflow:
    name: Optional[str] = None
    rules: Optional[List[WorkflowRule]] = None


@dataclasses.dataclass
class BuiltPipeline:
    # TODO: Should extend AnnotatedSource
    full_name: str
    original_name: str
    stages: OrderedDict[str, None] = dataclasses.field(default_factory=OrderedDict)
    variables: Dict[str, str] = dataclasses.field(default_factory=dict)
    # TODO: Workflows
    workflow: List[Dict[str, str]] = dataclasses.field(default_factory=list)
    # TODO: Need Rules
    jobs: List[BuiltJob] = dataclasses.field(default_factory=list)

    def add_job(self, job: BuiltJob, stage: Optional[str]) -> None:
        if stage:
            job.stage = stage
        if job.stage:
            self.stages[job.stage] = None
        # TODO: prob need to finagle variables once I figure out how we're going to do that.
        self.jobs.append(job)

    def add_pipeline(self, pipeline: BuiltPipeline, stage: Optional[str]) -> None:
        for job in pipeline.jobs:
            self.add_job(job, stage=stage)
        self.variables.update(pipeline.variables)
        self.workflow.extend(pipeline.workflow)

    def build_yaml(self) -> str:
        yaml = ruamel.yaml.YAML()  # typ="rtsc"
        yaml.indent(sequence=4, offset=2)
        yaml.allow_duplicate_keys = False

        final = C_MAP(
            stages=C_SEQ(self.stages.keys()),
            variables=C_MAP(self.variables.items()),
            workflows=C_SEQ(self.workflow),
        )
        for job in self.jobs:
            head_key = len(final)
            # By doing this super verbosely instead of using dataclasses.asdict
            # we can control the property order. This makes the YAML look
            # nicer as the underlying storage of C_MAP is an OrderedDict.
            job_map = C_MAP()
            if job.stage:
                job_map["stage"] = job.stage
            if job.allow_failure:
                job_map["allow_failure"] = job.allow_failure
            if job.coverage:
                job_map["coverage"] = job.coverage
            if job.interruptible:
                # noinspection SpellCheckingInspection
                job_map["interruptible"] = job.interruptible
            if job.rules:
                rules_seq = C_SEQ()
                job_map["rules"] = rules_seq
                for i, rule in enumerate(job.rules):
                    rules_seq.append(rule.to_yaml_dict())
                    if rule.source:
                        rules_seq.yaml_set_comment_before_after_key(
                            i, f"{rule.source} i={i}"
                        )
            if job.tags:
                tags_seq = C_SEQ()
                job_map["tags"] = tags_seq
                for i, tag in enumerate(job.tags):
                    tags_seq.append(tag.data)
                    if tag.source:
                        tags_seq.yaml_set_comment_before_after_key(i, tag.source)

            # Add script and after_script
            for key, job_scripts in [
                ("script", job.script),
                ("after_script", job.after_script),
            ]:
                scripts = C_SEQ()
                job_map[key] = scripts
                last_source = None
                for script in job_scripts:
                    idx = len(scripts)
                    scripts.extend(script.data)
                    if script.source and script.source != last_source:
                        last_source = script.source
                        scripts.yaml_set_comment_before_after_key(
                            idx, script.source, indent=4
                        )

            final[job.full_name] = job_map
            final.yaml_set_comment_before_after_key(head_key, job.full_name)

        stream = ruamel.yaml.compat.StringIO()
        yaml.dump(final, stream)
        return stream.getvalue()


class Pipeline(Buildable[BuiltPipeline]):
    def __init__(self, name: Optional[str] = None, *, stage: Optional[str] = None):
        name = (name or "").strip()
        super().__init__(name or f"~pipeline-{next(_global_pipeline_counter)}")
        self._stage: Optional[str] = stage
        self._children: List[Union[Pipeline, Job]] = list()

    def add_children(self, *children: Union[Job, Pipeline]) -> None:
        self._children.extend(children)

    # def build_yaml(
    #     self, user_kwargs: Optional[UserKwargs] = None, ns: Tuple[str, ...] = tuple()
    # ) -> str:
    #     yaml = ruamel.yaml.YAML()  # typ="rtsc"
    #     yaml.indent(sequence=4, offset=2)
    #     yaml.allow_duplicate_keys = False
    #
    #     pipeline_dict, jobs = self.build(user_kwargs or dict(), ns)
    #     final = C_MAP(**pipeline_dict)
    #
    #     for job in jobs:
    #         final[job.full_name] = C_MAP()
    #         job_dict = final[job.full_name]
    #         if job.stage:
    #             job_dict["stage"] = job.stage
    #
    #         # Add scripts
    #         for key, script in [
    #             ("script", job.script),
    #             ("after_script", job.after_script),
    #         ]:
    #             if not script:
    #                 continue
    #             job_dict[key] = C_SEQ()
    #             for head, lines in job.script:
    #                 head_key = len(job_dict[key])
    #                 for line in lines:
    #                     job_dict[key].append(line)
    #                 job_dict[key].yaml_set_comment_before_after_key(
    #                     head_key, f"Source job: {head}", 4
    #                 )
    #
    #     stream = ruamel.yaml.compat.StringIO()
    #     yaml.dump(final, stream)
    #     return stream.getvalue()

    def __call__(
        self, ctx: BuildContext, _build_stack: Tuple[BuildNamed, ...] = ()
    ) -> BuiltPipeline:
        new_build_stack = _build_stack + (self,)
        bp = BuiltPipeline(
            full_name=self.name_from_build_stack(new_build_stack),
            original_name=self.name,
        )

        for child in self._children:
            if isinstance(child, Job):
                bp.add_job(child(ctx, new_build_stack), self._stage)
            elif isinstance(child, Pipeline):
                bp.add_pipeline(child(ctx, new_build_stack), self._stage)
            else:
                raise TypeError(
                    f"Unknown type for Pipeline child. Expected Job or Pipeline, got: {type(child)}"
                )

        return bp
