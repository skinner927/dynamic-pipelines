from __future__ import annotations

import dataclasses
from typing import Dict, Iterable, List, OrderedDict

from dypi.models.job import BuiltJob


@dataclasses.dataclass()
class BuildResult:
    global_default: Dict[str, str] = dataclasses.field(default_factory=dict)
    """Global "default" mapping"""
    # Don't support include on purpose. Include order does not matter either.
    global_stages: OrderedDict[str, None] = dataclasses.field(
        default_factory=OrderedDict
    )
    """Global "stages" list"""
    global_variables: Dict[str, str] = dataclasses.field(default_factory=dict)
    """Global "stages" list"""
    jobs: List[BuiltJob] = dataclasses.field(default_factory=list)

    def update(self, others: Iterable[BuildResult]) -> None:
        """Merge other BuildResults into this BuildResult."""
        for item in others:
            self.global_default.update(item.global_default)
            for key in item.global_stages.keys():
                self.global_stages[key] = None
            self.global_variables.update(item.global_variables)
            self.jobs.extend(item.jobs)
