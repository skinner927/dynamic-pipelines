import dataclasses
import typing as t

_arg_none = object()


@dataclasses.dataclass()
class Argument:
    name: str
    type: type = str
    default: t.Any = _arg_none
    description: str = ""
    required: bool = False
    # One day
    # choices: t.Optional[t.Container[t.Any]] = None
