import collections
from typing import Protocol


class AnnotatedSource(Protocol):
    source: str


class AnnotatedSourceString(collections.UserString, AnnotatedSource):
    def __init__(self, init_str: str, /, source: str):
        super().__init__(init_str)
        self.data = init_str
        self.source = source
        if not self.source:
            raise ValueError("source cannot be empty")
