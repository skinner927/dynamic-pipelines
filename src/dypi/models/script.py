from __future__ import annotations

import collections
import typing
from typing import List, Optional, Sequence, Tuple

from dypi.models.annotations import AnnotatedSource
from dypi.models.build import Buildable, BuildContext, BuildNamed

# TODO: This is a stupid name
# ScriptArgs = Union[str, Iterable[str], Callable[[], str]]
ScriptArgs = str

if typing.TYPE_CHECKING:
    # "from __future__ import annotations" is supposed to fix this, but it's
    # not
    UserListStr = collections.UserList[str]
else:
    UserListStr = collections.UserList


class BuiltScript(UserListStr, AnnotatedSource):
    """A final representation of a collection of scripts annotated with their
    source.

    Treat this class like a List (because it is).
    """

    def __init__(self, init: Sequence[str], /, full_script_name: str) -> None:
        super().__init__(init)
        self.source = full_script_name


class Script(Buildable[BuiltScript]):
    """Represents one or more lines in a script.

    I know this looks overly complicated right now, but it's going to
    pay later... I think.
    """

    # def __init__(self, val: ScriptArgs) -> None:
    #     self.callback = Optional[Callable[[], str]] = None
    #     self.scripts: List[str] = list()
    #
    #     if isinstance(val, str):
    #         self.scripts.append(val)
    #     elif isinstance(val, Iterable):
    #         self.scripts.extend(val)
    #     elif callable(val):
    #         self.callback = val
    #     else:
    #         raise TypeError(f"Unexpected type for 'val' {type(val)}")

    def __init__(
        self, scripts: Optional[ScriptArgs] = None
    ):  # TODO: Change name to proper script repr
        super().__init__("script")
        self.scripts: List[str] = list()
        if scripts:
            self.scripts.append(scripts)

    def __call__(
        self, ctx: BuildContext, _build_stack: Tuple[BuildNamed, ...] = ()
    ) -> BuiltScript:
        return BuiltScript(
            self.scripts.copy(),
            full_script_name=self.name_from_build_stack(_build_stack + (self,)),
        )
