from typing import Optional, Dict


class Registry:
    """A registry is used to associate Pipelines and Jobs together into related
    URLs.

    The purpose of the registry is to ensure there are no name
    collisions and to assist rendering the user interface.
    """

    def __init__(self, name: Optional[str] = None):
        name = (name or "").strip()
        self._name = name
        if not self._name:
            raise ValueError("Registry name cannot be empty")

        self._root_pipelines: Dict[str, str] = dict()
