import os
import secrets

from flask import Blueprint, Flask, flash, render_template

bp = Blueprint("views", __name__, url_prefix="/")


@bp.route("/")
def list_pipelines() -> str:
    # flash("lol what")
    return render_template("list-pipelines.html", yaml_dump=dict())


def create_app() -> Flask:
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(SECRET_KEY=secrets.token_bytes(64))

    app.register_blueprint(bp)
    app.add_url_rule("/", endpoint="index")

    # No point right now
    # os.makedirs(app.instance_path, exist_ok=True)

    return app
