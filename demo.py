import dypi
from dypi.models.build import BuildContext


def main() -> None:
    job1 = dypi.Job("job1")
    job1.script.append("This is line 1 in script 1")
    job1.script.append("This is line 2 in script 1")

    job2 = dypi.Job("job2")
    job2.script.append("This is script2 line 1")
    job2.script.append("This is script2 line 2")

    job3 = dypi.Job("job3")
    job3.extends(job1, job2)
    job3.script.prepend("job3 prepend 0")

    pipe1 = dypi.Pipeline("pipe1")
    pipe1.add_children(job1, job2, job3)

    ctx = BuildContext()
    print(pipe1(ctx).build_yaml())


if __name__ == "__main__":
    main()
